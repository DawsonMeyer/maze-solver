%I got this idea from a couple of projects I had seen on the internet
%they can be found here:

%https://github.com/muratkrty/reinforcement-learning-robot-in-maze

%https://www.mathworks.com/matlabcentral/fileexchange/63062-maze-solver--
%reinforcement-learning-

%https://www.mathworks.com/matlabcentral/fileexchange/63407-reinforcement
%earning--q-learning


%directions are going to be:
% North = 1        1
% East = 2      4     2
% South = 3        3
% West = 4

function MazeLearning
clear;
close all;
global maze;
global tempMaze;

textFile = 'maze-9x9.txt'; 
[maze, row, col] = convertMaze(textFile);
image(maze)
colormap jet;
pause(1);

%making tempMaze so we can show the progress when its trying to solve
tempMaze = maze;
[goalR,goalC] = find(tempMaze == 100); %end position

%We need the starting pos of the mouse
strtX = row;
strtY = col;

%There are a lot of different states that we need to look at as it is 
%learning; there are 4 actions for every state so we need 4 state tables.
%each state table keeps the data for one direction that the mouse can move
States = zeros(size(maze, 1), size(maze, 2), 4);

%do 50 iterations 
for i = 1:50
    row = strtX;
    col = strtY;
    currentDir = 1; 
    status = -1; 
    count = 0;
    
    %We need it to traverse the maze every iteration
    %when the status is 3 it means we're at the end so we are done with
    %that iteration
    while status ~= 3
        
        %the previous spot of the mouse so we can update the state tables 
        %based on where the mouse was, and what move the mouse made
        pR = row;
        pC = col;
        prevDist = 100;
     
        %select the maximum value of the states to decide where we want it
        %to move
        [val, idx] = max(States(row, col, :)); %for all of the possible dir'n
        y = find(States(row,col,:) == val); %then find what direction that is 
        if size(y,1) > 1 %if there are multiple places that are valid
            idx = 1+round(rand*(size(y,1)-1));%choose one of them to move to
            move = y(idx,1); %this position is what we decided will be the move we make
        else
            move = idx;
        end
        
        %turn the mouse to face the correct direction
        while currentDir ~= move
            currentDir = turnRight(currentDir);
            % count the actions required to reach the goal
            count = count + 1;            
        end
        
        %Now it moves ahead after we decided that it's a possible move
        [row,col,status] = moveToNextSpace(row, col, currentDir);
        count = count + 1;
        
        %Dispays the hamming distance to the end, not the 
        %distance to the finish along the path
        X = [row, col];
        Y = [goalR, goalC];   
        dist = norm(X-Y,1);        
        
        if status == 2 %hit a wall
            reward = -2; %that's a bad move
        elseif status == 3 %ht the end
            reward = 2; %that is a good move
        else 
            reward = 1; %didn't hit a wall is good
                        %but not as good as the end
        end
        
        %if the previous distance from the end is less
        %then get slightly punished
        if status ~= 3 && prevDist > dist
            reward = reward-1;
        end
        
        %update mouse info in the state table
        %we update the infor based on where we were is equal to where we
        %were and what move we took to maximize the state we went to.
        %the .8 and .5 values are put in for the learning rates
        %it seems like these are a good balance.  if they are taken out,
        %it learns too fast.
        States(pR,pC,move) = 0.8*(reward+0.5*max(States(row,col,:)));
        
        %display the maze
        prevDist = dist;
        image(tempMaze);     
        title(sprintf('Reinforcement Learning'));
        drawnow
        
    end
    
    tempMaze = maze; %restore the colours
    numMoves(i,1) = count;
  
    %display the final maze
    imagesc(maze);
    colormap jet;
    fprintf('It took: %d moves', count);
    drawnow;
    
end
end

%need the mouse to be able to change direction
function curDir = turnRight(curDir)
    if curDir == 1
        curDir = 2;
    elseif curDir == 2
        curDir = 3;
    elseif curDir == 3
        curDir = 4;
    else
        curDir = 1;
    end
end


%We want the mouse to move to the next location if its safe to do so 
function [row,col,status] = moveToNextSpace(row,col,currentDir)  
global tempMaze;

% based on the current direction check whether next location is a valid
% move or if its a wall
[val,valid] = LookForward(row,col,currentDir);

%check that the space ahead is good
%if not then wall was hit
if valid == 1
    % now check if the next location
    % this is for walls inside the maze
    if val > 0
        oldRow = row;
        oldCol = col;
        if currentDir == 1
            row = row - 1;
        elseif currentDir == 2 
            col = col + 1; 
        elseif currentDir == 3 
            row = row + 1; 
        elseif currentDir == 4 
            col = col - 1;
        end
        status = 1;        
        
        if val == 100
            %got to the end
            status = 3;
            disp(" ");
            disp("End Reached!");
        end
        
        %change the colour so we can see it learning and the path it took
        tempMaze(oldRow,oldCol) = 45;                 
        tempMaze(row,col) = 60; 
        
    elseif val == 0
        %hit wall
        status = 2;        
    end
else
    status = 2;
end 
end

%return the information just in front of the mouse regardles of direction
%we want to know if the space ahead is valid, and what value it has 
function [value,valid] = LookForward(row,col,currentDir)  
global maze;
valid = 0;

%0's are the values for the walls in the maze so if its any other value
%than that, its a valid move.
if currentDir == 1
    if row-1 > 0
        value = maze(row-1,col);
        valid = 1;
    end
elseif currentDir == 2
    if col+1 > 0
        value = maze(row, col+1);
        valid = 1;
    end
elseif currentDir == 3
    if row+1 > 0
        value = maze(row+1,col);
        valid = 1;
    end
elseif currentDir == 4
    if col-1 > 0 
        value = maze(row,col-1);
        valid = 1;
    end
end
end

