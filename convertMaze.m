%this is the file that reads in the maze from text file

function [maze, row, col] = convertMaze(textFile)

%takes the textfile and reads the data to put into
%array A.  Takes out the delimeter charachter ',' 
%from the data as well.
[A,delimiterOut] = importdata(textFile);

%only solves a 9x9 maze for now, but could be changed
%forlarger ones in the future.
for i=1:9
    for j=1:9
        if A(i,j) == 1
            %valid move space
            maze(i,j) = 50;
            
        elseif A(i,j) == 2
            %start space
            maze(i,j) = 60;
            row = i;
            col = j;
            
        elseif A(i,j) == 3
            %end space
            maze(i,j) = 100;
            
        else
            %its a wall
            maze(i,j) = 0;
        end
    end
end


